const EquiposFV = require("../models/EquiposFV")



exports.obtenerEquiposFV= async (req, res) => {
try {
  const equiposfv = await EquiposFV.find()
  res.json(equiposfv)
} catch (error) {
  res.json(error)  
}
  }

exports.obtenerunEquiposFV= async (req, res) => {
  try {
    const {No_Parte,Corriente} = req.body

    if (No_Parte && Corriente){      
      const equiposfv = await EquiposFV.findOne({No_Parte},{Corriente}).exec()
      if (!equiposfv) {
        res.json({msj:"Error en la Busqueda"})     
        
      } else {
        
        res.json({msj:"Busqueda Satisfactoria"})  
      }       
    
    } else {
      res.json({isOk: false, msj: "Se requieren datos"})
    }
    
  } catch (error) {
  res.json(error)
    
  }
    }

exports.agregarEquiposFV = async (req, res) => {
  try {
    const {Unidad, Cantidad,No_Parte,Descripcion,Voltaje, Corriente} = req.body   

    if (Unidad && Cantidad && No_Parte && Descripcion && Voltaje && Corriente){
      const nuevoUsuario = new EquiposFV({Unidad, Cantidad, No_Parte, Descripcion, Voltaje, Corriente})
      await nuevoUsuario.save()
      res.json({msj:"documento insertado satisfactoriamente"})
    } else {
      res.json({isOk: false, msj: "Los datos son requeridos"})
    }
    
  } catch (error) {
    res.json(error)   
  }
  }

exports.actualizarEquiposFV = async (req, res) => {
try {
  const id = req.params.id
  const data = req.body
  

  if (id && data){
    await EquiposFV.findByIdAndUpdate(id, data)    
    res.json("Registro actualizado")    
  } else {
    res.json({msj:"Registro no actualizado"})     
  }
} catch (error) {
  res.json(error)
}
  }

    
exports.eliminarEquiposFV = async (req, res) => {
try {
  const id = req.params.id
  console.log(id)
  const eliminado = await EquiposFV.findByIdAndDelete(id)
  res.status(200).json({msj:"id recibida para eliminiar usuario", isOk: true})
} catch (error) {
  res.status(500).json(error)
}
  }


