const {Schema, model} = require("mongoose")

const EquiposFVSchema = new Schema(
    {
        Unidad: {
            type: String,
            required: false
        },
        Cantidad: {
            type: Number,
            required: true
        },
        No_Parte: {
            type: String,
            required: true
        },
        Descripcion: {
            type: String,
            required: true
        },
        Voltaje: {
            type: Number,
            required: true
        },
        Corriente: {
            type: Number,
            required: true
        }        
    }
)

module.exports = model("EquiposFV", EquiposFVSchema)